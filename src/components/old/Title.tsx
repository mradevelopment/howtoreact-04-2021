import React from 'react'

const Title = ({ title }: { title: string }) => <h1>{title}</h1>

export { Title }