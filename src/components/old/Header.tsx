import React from 'react'

export const Header: React.FC = ({ children }) => <header className="htr-header">{children}</header>