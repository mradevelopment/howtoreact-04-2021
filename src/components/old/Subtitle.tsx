import React from 'react'

export const Subtitle = ({ subtitle }: { subtitle: string }) => <small>{subtitle}</small>