import React from 'react'

export const Footer: React.FC = ({ children }) => <footer className="htr-footer">{children}</footer>