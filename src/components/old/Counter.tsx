import React, { useEffect } from 'react'
import { Title } from './Title'

interface CounterProps {
    count: number
    onCounterClicked: () => void
}

export const Counter = (props: CounterProps) => {

    useEffect(() => {
        return () => {
          console.log("Count has been removed from the DOM")
        }
      }, [])

    const onClick = () => {
        props.onCounterClicked()
    }

    return (
        <div>
            <Title title={`Count: ${props.count}`} />
            <button onClick={onClick}>Count</button>
        </div>
    )
}