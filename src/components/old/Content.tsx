import React from 'react'

export const Content: React.FC = ({ children }) => <div className="htr-content">{children}</div>
