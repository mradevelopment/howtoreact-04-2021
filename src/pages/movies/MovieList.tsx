import React from "react"
import { RouteComponentProps } from "react-router"
import "./MovieList.css"

export interface Movie {
    id: number
    title: string
    year: string
    runtime: string
    genres: string[]
    director: string
    actors: string
    plot: string
    posterUrl: string
}

interface MovieListProps extends RouteComponentProps {
    movies: Movie[]
}

const MovieList = ({ movies, history, match }: MovieListProps) => {
    return (
        <div className="movie-list">
            {
                movies.map(movie => (
                    <div className="movie-list-item" onClick={() => history.push(`${match.path}/${movie.id}`)}>
                        <h2>{movie.title}</h2>
                        <img src={movie.posterUrl} />
                    </div>
                ))
            }
        </div>
    )
}

export { MovieList }