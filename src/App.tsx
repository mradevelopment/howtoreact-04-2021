import React, { useEffect, useState } from 'react';
import './App.css';

import { Content } from './components/old/Content';
import { Footer } from './components/old/Footer';
import { Header } from './components/old/Header';
import { Subtitle } from './components/old/Subtitle';
import { Title } from './components/old/Title';
import { Movie, MovieList } from "./pages/movies/MovieList"
import datasource from "./data/datasource.json"
import { Redirect, Route, RouteComponentProps, Switch } from 'react-router';

interface MovieRouteComponentProps extends RouteComponentProps<{ movieId: string }> {

}

const MovieRoutes = ({ match }: MovieRouteComponentProps) => (
  <Switch>
    <Route exact={true} path={"/movies"} render={routeComponentProps => <MovieList movies={datasource.movies} {...routeComponentProps} />} />
    <Route path={`movies/:movieId`} render={(routeComponentsProps: RouteComponentProps<{ movieId: string }>) => {
      console.log("Render movie details")
      const movieId = Number(routeComponentsProps.match.params.movieId)
      if (isNaN(movieId)) {
        return <Redirect to="/" />
      }

      const movie = datasource.movies.find(m => m.id === movieId)

      if (movie) {
        return (
          <div>
            <h1>{movie.title}</h1>
          </div>
        )
      } else {
        return <Redirect to="/" />
      }
    }} />

  </Switch>
)

function App() {
  return (
    <div className="App">
      <Header>
        <Title title="How to React" />
      </Header>
      <Content>
        <Switch>
          <Route strict={true} path="/movies" component={MovieRoutes} />
        </Switch>
      </Content>
      <Footer>
        <Subtitle subtitle="I'm now a Senior React Engineer" />
      </Footer>
    </div>
  );
}

export default App;
